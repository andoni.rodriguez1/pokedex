import { createContext, useContext, useMemo, useState } from "react";
import PropTypes from "prop-types";

const StepperContext = createContext({
  teamData: {
    teamName: "",
    description: "",
    teamType: "",
    region: {
      name: "",
      id: "",
    },
    pokemons: [],
  },
  setTeamData: () => {},
  mode: "",
  setMode: () => {},
});

export function StepperContextProvider({ children }) {
  const [teamData, setTeamData] = useState({
    teamName: "",
    description: "",
    teamType: "",
    region: {
      name: "",
      id: "",
    },
    pokemons: [],
  });

  const [mode, setMode] = useState("");

  const teamContextValue = useMemo(
    () => ({ teamData, setTeamData, mode, setMode }),
    [teamData, setTeamData, mode, setMode]
  );

  return (
    <StepperContext.Provider value={teamContextValue}>
      {children}
    </StepperContext.Provider>
  );
}

export function useStepperContext() {
  const { teamData, setTeamData, mode, setMode } = useContext(StepperContext);

  return { teamData, setTeamData, mode, setMode };
}

StepperContextProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
