const pokemonAbilitiesAdapter = (ability) => ({
  name: ability.pokemon_v2_ability.name,
  effect:
    ability.pokemon_v2_ability.pokemon_v2_abilityeffecttexts[0]?.short_effect,
});

export default pokemonAbilitiesAdapter;
