const pokemonSpritesAdapter = (sprites) => ({
  front_default: JSON.parse(sprites).front_default,
});

export default pokemonSpritesAdapter;
