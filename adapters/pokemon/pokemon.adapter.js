import pokemonAbilitiesAdapter from "./pokemonAbilities.adapter";
import pokemonStatsAdapter from "./pokemonStats.adapter";
import pokemonTypesAdapter from "./pokemonTypes.adapter";
import pokemonSpritesAdapter from "./pokemonSprites.adapter";

const pokemonAdapter = (pokemon) => ({
  id: pokemon.pokemon_species_id,
  name: pokemon.pokemon_v2_pokemonspecy.name,
  abilities:
    pokemon.pokemon_v2_pokemonspecy.pokemon_v2_pokemons[0]?.pokemon_v2_pokemonabilities.map(
      (ability) => pokemonAbilitiesAdapter(ability)
    ),
  stats:
    pokemon.pokemon_v2_pokemonspecy.pokemon_v2_pokemons[0]?.pokemon_v2_pokemonstats.map(
      (stat) => pokemonStatsAdapter(stat)
    ),
  types:
    pokemon.pokemon_v2_pokemonspecy.pokemon_v2_pokemons[0]?.pokemon_v2_pokemontypes.map(
      (type) => pokemonTypesAdapter(type)
    ),

  sprites: pokemonSpritesAdapter(
    pokemon.pokemon_v2_pokemonspecy.pokemon_v2_pokemons[0]
      ?.pokemon_v2_pokemonsprites[0]?.sprites
  ),
});

export default pokemonAdapter;
