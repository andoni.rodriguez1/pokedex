const pokemonTypesAdapter = (pokemonType) => ({
  name: pokemonType.pokemon_v2_type.name,
});

export default pokemonTypesAdapter;
