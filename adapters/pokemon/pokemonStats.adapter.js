const pokemonStatsAdapter = (stat) => ({
  name: stat.pokemon_v2_stat.name,
  base_stat: stat.base_stat,
});

export default pokemonStatsAdapter;
