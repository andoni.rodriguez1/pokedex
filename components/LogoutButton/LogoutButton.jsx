import { signOut } from "firebase/auth";
import { auth } from "@/config/firebase.config";
import Button from "@/components/Button";
import { BiLogOut } from "react-icons/bi";

function LogoutButton() {
  const handleLogout = async () => {
    await signOut(auth);
  };

  return (
    <Button
      className="bg-zinc-800 text-white hover:bg-zinc-700"
      onClick={handleLogout}
      startIcon={<BiLogOut />}
    >
      Sign-out
    </Button>
  );
}

export default LogoutButton;
