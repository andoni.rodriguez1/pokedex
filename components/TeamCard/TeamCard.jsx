import { useState } from "react";
import PropTypes from "prop-types";
import Image from "next/image";
import TypeBadge from "@/components/TypeBadge/TypeBadge";
import { useStepperContext } from "@/context/StepperContext";
import Modal from "@/components/Modal";
import Link from "next/link";
import PokemonDetailView from "@/components/PokemonDetailVew";
import TeamDetailView from "@/components/TeamDetailView";
import { BsThreeDots } from "react-icons/bs";
import Button from "@/components/Button";

const STATS_TO_MAP = {
  hp: "hp",
  attack: "attack",
};

function TeamCard({ team }) {
  const { setTeamData, setMode } = useStepperContext();
  const [isPokemonDetailModalOpen, setIsPokemonDetailModalOpen] =
    useState(false);
  const [isTeamDetailModalOpen, setIsTeamDetailModalOpen] = useState(false);
  const [selectedPokemon, setSelectedPokemon] = useState({
    name: "",
  });

  return (
    <div className="w-full max-w-md p-4 border rounded-lg shadow-md  bg-zinc-800 border-zinc-700 mx-auto h-full">
      <div className="flex items-center justify-between mb-4 pl-4 pt-2">
        <h5 className="text-xl font-bold leading-none">{team.teamName}</h5>
        <Button
          className="hover:bg-zinc-700"
          onClick={() => setIsTeamDetailModalOpen(!isTeamDetailModalOpen)}
        >
          <BsThreeDots size={18} />
        </Button>
      </div>
      <div className="flex flex-col h-5/6">
        <ul className="flex flex-col sm:grid grid-cols-2">
          {team.pokemons.map((pokemon) => (
            <li
              key={pokemon.name}
              className="py-3 px-4 rounded-lg hover:bg-zinc-700"
            >
              <button
                className="w-full"
                type="button"
                onClick={() => {
                  setSelectedPokemon(pokemon);
                  setIsPokemonDetailModalOpen(!isPokemonDetailModalOpen);
                }}
              >
                <div className="flex items-center space-x-4">
                  <div className="flex flex-col grow min-w-0">
                    <p className="text-sm font-medium capitalize flex gap-3">
                      {pokemon.name}
                    </p>
                    <div className="flex flex-wrap gap-2 items-center text-base font-semibold pt-5">
                      {pokemon.stats.map((stat) => {
                        if (STATS_TO_MAP[stat.name]) {
                          return (
                            <span
                              className="text-zinc-500 uppercase text-xs"
                              key={stat.name}
                            >
                              {stat.name}: {stat.base_stat}
                            </span>
                          );
                        }
                        return null;
                      })}
                    </div>
                  </div>
                  <div className="flex-shrink-0">
                    <Image
                      className="w-16 h-16 rounded-full mx-auto"
                      src={pokemon.sprites.front_default}
                      alt="Neil image"
                      width={144}
                      height={144}
                    />
                    <div className="flex flex-col gap-2 items-center text-base font-semibold">
                      {pokemon.types.map((type) => (
                        <TypeBadge key={type.name} type={type.name}>
                          {type.name}
                        </TypeBadge>
                      ))}
                    </div>
                  </div>
                </div>
              </button>
            </li>
          ))}
        </ul>
        <div className="flex grow items-end">
          <Link
            className=" px-4 mx-auto py-2 w-fit flex items-center justify-start gap-4 rounded-lg bg-zinc-200 text-black font-semibold hover:bg-zinc-50 uppercase"
            href="/edit-team"
            onClick={() => {
              setMode("edit");
              setTeamData(team);
            }}
          >
            edit team
          </Link>
        </div>
      </div>
      <Modal
        title={selectedPokemon.name}
        showModal={isPokemonDetailModalOpen}
        setShowModal={setIsPokemonDetailModalOpen}
      >
        <PokemonDetailView pokemonName={selectedPokemon.name} />
      </Modal>
      <Modal
        title={team.teamName}
        showModal={isTeamDetailModalOpen}
        setShowModal={setIsTeamDetailModalOpen}
      >
        <TeamDetailView team={team} />
      </Modal>
    </div>
  );
}

TeamCard.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  team: PropTypes.object.isRequired,
};

export default TeamCard;
