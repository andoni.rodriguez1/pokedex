import { StepperContextProvider } from "@/context/StepperContext";

const WithTeamCreationContext = (Component) => {
  function context(props) {
    return (
      <StepperContextProvider>
        {/* eslint-disable-next-line react/jsx-props-no-spreading */}
        <Component {...props} />
      </StepperContextProvider>
    );
  }

  return context;
};

export default WithTeamCreationContext;
