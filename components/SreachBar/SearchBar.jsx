import PropTypes from "prop-types";

function SearchBar({ onChange }) {
  return (
    <label className="flex gap-4 items-center w-fit" htmlFor="pokemon">
      Search:
      <input
        className="bg-transparent border border-zinc-500 rounded-md py-1 px-3 focus:outline-none focus:backdrop-blur-md"
        type="text"
        onChange={onChange}
      />
    </label>
  );
}

SearchBar.propTypes = {
  onChange: PropTypes.func.isRequired,
};

export default SearchBar;
