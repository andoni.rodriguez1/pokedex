import PropTypes from "prop-types";
import TeamCard from "@/components/TeamCard";

function TeamList({ teams }) {
  return (
    <ul className="grid grid-cols-1 gap-10 pt-12 lg:grid-cols-2  xl:grid-cols-3">
      {teams.map((team) => (
        <li key={team.id}>
          <TeamCard team={team} />
        </li>
      ))}
    </ul>
  );
}

TeamList.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  teams: PropTypes.array.isRequired,
};

export default TeamList;
