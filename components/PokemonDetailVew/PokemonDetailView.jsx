import PropTypes from "prop-types";
import { useQuery } from "react-query";
import Image from "next/image";
import getPokemonByName from "@/queries/getPokemonByName";
import TypeBadge from "@/components/TypeBadge/TypeBadge";
import NoPokemonImage from "@/assets/images/no_pokemon_image.png";
import { RiLoader4Line } from "react-icons/ri";

function PokemonDetailView({ pokemonName }) {
  const loadPokemon = () => getPokemonByName(pokemonName);
  const { data: pokemon, isLoading } = useQuery(["pokemon"], loadPokemon, {
    select: (data) => data.data,
  });

  if (isLoading) {
    return <RiLoader4Line size={24} className="animate-spin flex w-full" />;
  }

  return (
    <div className="max-w-xs rounded-lg bg-zinc-800 mx-auto">
      <div className="flex flex-col items-center pb-10">
        <div className="pt-2">
          <Image
            src={pokemon.sprites.front_default || NoPokemonImage}
            alt={pokemon.name}
            width={144}
            height={144}
            defaultValue={NoPokemonImage}
          />
        </div>
        <div className="flex flex-col gap-4 items-center">
          <div className="flex flex-wrap justify-center gap-2">
            {pokemon.types.map((type) => (
              <TypeBadge key={type.type.name} type={type.type.name} />
            ))}
          </div>
        </div>

        <div className="flex flex-col gap-2 items-start justify-start pt-5 w-full uppercase">
          <span className="font-semibold tracking-widest">stats:</span>
          <div className="grid grid-cols-2 items-center gap-2 rounded-lg bg-zinc-700 p-4 w-full text-xs">
            {pokemon.stats.map((stat) => (
              <p key={stat.stat.name} className="font-semibold">
                {stat.stat.name}:{" "}
                <span className="text-yellow-400">{stat.base_stat}</span>
              </p>
            ))}
          </div>
        </div>

        <div className="flex flex-col gap-2 items-start justify-start pt-5 w-full uppercase">
          <span className="font-semibold tracking-widest">abilities:</span>
          <div className="grid grid-cols-2 items-center gap-2 rounded-lg bg-zinc-700 p-4 w-full text-xs">
            {pokemon.abilities.map((ability) => (
              <p
                key={ability.ability.name}
                className="font-semibold text-cyan-400"
              >
                {ability.ability.name}
              </p>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

PokemonDetailView.propTypes = {
  pokemonName: PropTypes.string.isRequired,
};

export default PokemonDetailView;
