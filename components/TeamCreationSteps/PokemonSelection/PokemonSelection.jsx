import { useState } from "react";
import request from "graphql-request";
import { useQuery } from "react-query";
import getAllPokemonByRegion from "@/queries/getAllPokemonByRegion.query";
import { useStepperContext } from "@/context/StepperContext";
import pokemonAdapter from "@/adapters/pokemon/pokemon.adapter";
import Pokemon from "@/components/Pokemon/Pokemon";
import Button from "@/components/Button/Button";
import Modal from "@/components/Modal";
import TeamPreview from "@/components/TeamPreview";

function PokemonSelection() {
  const { teamData, setTeamData } = useStepperContext();
  const [isPreviewModalOpen, setIsPreviewModalOpen] = useState(false);

  const loadPokemons = () =>
    request(
      process.env.NEXT_PUBLIC_POKEAPI_GRAPHQL_URL,
      getAllPokemonByRegion(teamData.region.id)
    );

  const { data: pokemons, isLoading } = useQuery(["pokemons"], loadPokemons, {
    select: (data) =>
      data.pokemon_v2_region_by_pk.pokemon_v2_pokedexes[0].pokemon_v2_pokemondexnumbers_aggregate.nodes?.map(
        (pokemon) => pokemonAdapter(pokemon)
      ),
  });

  const handlePokemonAddToTeam = (pokemon) => {
    setTeamData((prev) => ({
      ...prev,
      pokemons: [...prev.pokemons, pokemon],
    }));
  };

  const handlePokemonRemoveFromTeam = (pokemonIdx) => {
    const result = teamData.pokemons;
    result.splice(pokemonIdx, 1);
    setTeamData((prev) => ({
      ...prev,
      pokemons: result,
    }));
  };

  const isPokemonOnTeam = (pokemon) =>
    teamData.pokemons.some((teamPokemon) => teamPokemon.id === pokemon.id);

  if (isLoading) return <p>Loading...</p>;

  return (
    <section className="h-full overflow-auto scrollbar-hide">
      <div className="sticky top-14 mt-12 w-fit flex gap-5 mx-auto">
        <Button
          className="bg-zinc-200 text-black font-semibold uppercase hover:bg-zinc-50 "
          onClick={() => setIsPreviewModalOpen(!isPreviewModalOpen)}
        >
          Preview Members
        </Button>
        <Modal
          title={teamData.teamName}
          showModal={isPreviewModalOpen}
          setShowModal={setIsPreviewModalOpen}
        >
          <TeamPreview
            team={teamData.pokemons}
            onRemoveFromTeamClick={handlePokemonRemoveFromTeam}
          />
        </Modal>
      </div>
      {pokemons?.length > 0 ? (
        <ul
          className="grid grid-cols-1 gap-10 h-full pt-12 
          sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4"
        >
          {pokemons.map((pokemon) => (
            <li key={pokemon.id}>
              <Pokemon
                pokemon={pokemon}
                onAddToTeamClick={() => handlePokemonAddToTeam(pokemon)}
                addDisabled={teamData.pokemons.length >= 6}
                isPokemonOnTeam={isPokemonOnTeam(pokemon)}
              />
            </li>
          ))}
        </ul>
      ) : null}
    </section>
  );
}

export default PokemonSelection;
