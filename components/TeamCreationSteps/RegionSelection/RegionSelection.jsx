import WithPrivateRoute from "@/components/WithPrivateRoute";
import request from "graphql-request";
import { useQuery } from "react-query";
import getRegions from "@/queries/getAllRegions.query";
import { useStepperContext } from "@/context/StepperContext";

function RegionSelection() {
  const loadRegions = () =>
    request(process.env.NEXT_PUBLIC_POKEAPI_GRAPHQL_URL, getRegions);
  const { data, isLoading } = useQuery(["regions"], loadRegions);
  const { teamData, setTeamData } = useStepperContext();

  const handleRegionSelection = (e) => {
    const region = JSON.parse(e.target.value);
    setTeamData({ ...teamData, region, pokemons: [] });
  };

  if (isLoading) return <p>Loading...</p>;

  return (
    <section className="h-full overflow-auto">
      {data ? (
        <ul
          className="grid grid-cols-2 gap-10 h-full pt-12 px-12
          md:grid-cols-3 lg:grid-cols-4"
        >
          {data.pokemon_v2_region.map((region) => (
            <button
              className={`aspect-square w-full h-full max-w-xs max-h-xs bg-transparent relative text-center mx-auto rounded-md uppercase  
            hover:bg-zinc-800 ${
              teamData.region.name === region.name
                ? "border-2 border-white font-semibold"
                : "font-normal"
            } `}
              type="button"
              key={region.name}
              value={JSON.stringify(region)}
              onClick={handleRegionSelection}
            >
              {region.name}
            </button>
          ))}
        </ul>
      ) : (
        <p>Loading</p>
      )}
    </section>
  );
}

export default WithPrivateRoute(RegionSelection);
