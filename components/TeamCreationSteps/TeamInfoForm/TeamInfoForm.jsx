import { useStepperContext } from "@/context/StepperContext";

function TeamInfoForm() {
  const { teamData, setTeamData } = useStepperContext();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setTeamData((prev) => ({ ...prev, [name]: value }));
  };

  return (
    <form className="flex flex-col items-center justify-center gap-10 h-full w-full md:max-w-lg mx-auto">
      <label className="w-full" htmlFor="teamName">
        <div className="flex flex-col gap-1">
          Team Name
          <input
            className="bg-transparent border border-zinc-500 rounded-md w-full py-1 px-3
            focus:outline-none "
            type="text"
            id="teamName"
            name="teamName"
            value={teamData.teamName}
            required
            onChange={handleChange}
          />
        </div>
      </label>

      {/* team type select */}
      <label className="w-full" htmlFor="teamType">
        <div className="bg-transparent flex flex-col gap-1">
          Team Type
          <select
            className="border border-zinc-500 text-sm rounded-md block w-full p-2.5 bg-zinc-900"
            id="teamType"
            name="teamType"
            value={teamData.teamType}
            required
            onChange={handleChange}
          >
            <option value="attack">Attack</option>
            <option value="defense">Defense</option>
            <option value="tank">Tank</option>
            <option value="burst">Burst</option>
          </select>
        </div>
      </label>

      <label className="w-full h-40" htmlFor="teamDescription">
        <div className="flex flex-col gap-1 h-full">
          Team Description
          <textarea
            className="bg-transparent border border-zinc-500 rounded-md w-full h-full py-1 px-3
            focus:outline-none"
            type="text"
            id="description"
            name="description"
            value={teamData.description}
            required
            onChange={handleChange}
          />
        </div>
      </label>
    </form>
  );
}

export default TeamInfoForm;
