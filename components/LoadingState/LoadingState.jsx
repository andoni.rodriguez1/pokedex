import Lottie from "lottie-react";
import loadingLottie from "@/assets/lotties/loading.json";

function LoadingState() {
  return (
    <div className="grow overflow-auto items-center flex justify-center">
      <Lottie animationData={loadingLottie} loop />
    </div>
  );
}

export default LoadingState;
