import PropTypes from "prop-types";

function TeamDetailView({ team }) {
  return (
    <div className="flex flex-col gap-4 bg-zinc-700 p-5 rounded-lg">
      <div className="flex gap-2 justify-start items-center">
        <h2 className="uppercase font-semibold text-zinc-300">Region: </h2>
        <p className="capitalize text-cyan-500">{team.region.name}</p>
      </div>
      <div className="flex gap-2 justify-start items-center">
        <h2 className="uppercase font-semibold text-zinc-300">Type: </h2>
        <p className="capitalize text-cyan-500">{team.teamType}</p>
      </div>
      <div className="flex flex-wrap gap-2 justify-start items-center">
        <h2 className="uppercase font-semibold text-zinc-300">Description: </h2>
        <p className=" text-stone-400">{team.description}</p>
      </div>
    </div>
  );
}

TeamDetailView.propTypes = {
  team: PropTypes.shape({
    teamName: PropTypes.string,
    description: PropTypes.string,
    region: PropTypes.shape({
      name: PropTypes.string,
    }),
    teamType: PropTypes.string,
  }).isRequired,
};

export default TeamDetailView;
