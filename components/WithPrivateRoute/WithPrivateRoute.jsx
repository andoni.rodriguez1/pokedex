import { auth } from "@/config/firebase.config";
import { useAuthState } from "react-firebase-hooks/auth";
import { useRouter } from "next/router";

import { useEffect } from "react";

const WithPrivateRoute = (Component) => {
  function Auth(props) {
    const [user, loading] = useAuthState(auth);
    const router = useRouter();

    useEffect(() => {
      if (!loading && !user) {
        router.replace("/login");
      }
    }, [loading, user]);

    if (loading || !user) {
      return null;
    }

    // eslint-disable-next-line react/jsx-props-no-spreading
    return <Component {...props} />;
  }

  return Auth;
};

export default WithPrivateRoute;
