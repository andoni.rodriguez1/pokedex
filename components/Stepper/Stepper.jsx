import { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";

function Stepper({ steps, currentStep }) {
  const [newStep, setNewStep] = useState([]);
  const stepsRef = useRef();

  const updateStep = (stepNumber, stepsParam) => {
    const newSteps = [...stepsParam];

    const resultSetps = newSteps.map((step, index) => {
      if (index === stepNumber) {
        return {
          ...step,
          highlighted: true,
          selected: true,
          completed: false,
        };
      }

      if (index < stepNumber) {
        return {
          ...step,
          highlighted: false,
          selected: true,
          completed: true,
        };
      }
      return {
        ...step,
        highlighted: false,
        selected: false,
        completed: false,
      };
    });

    return resultSetps;
  };

  useEffect(() => {
    const stepsState = steps.map((step, index) => ({
      ...steps,
      description: step,
      completed: false,
      highlighted: index === 0,
      selected: index === 0,
    }));

    stepsRef.current = stepsState;
    const current = updateStep(currentStep, stepsRef.current);
    setNewStep(current);
  }, [steps, currentStep]);

  const stepsDisplay = newStep.map((step, index) => {
    const elementKey = index;
    return (
      <div
        key={elementKey}
        className={
          index !== newStep.length - 1
            ? "w-full flex items-center"
            : "flex items-center"
        }
      >
        <div className="relative flex flex-col items-center text-zinc-400">
          <div
            className={`transition duration-500 ease-in-out rounded-lg h-12 w-12 flex items-center justify-center py-3 ${
              step.selected && !step.completed
                ? "bg-zinc-700 border-white text-white font-bold border-2"
                : ""
            }`}
          >
            {step.completed ? (
              <span className="text-white font-bold text-xl">&#10003;</span>
            ) : (
              index + 1
            )}
          </div>
          <div
            className={`absolute top-0 text-center mt-16 w-30 text-xs font-medium uppercase ${
              step.highlighted ? "text-white" : "text-zinc-400"
            }`}
          >
            {step.description}
          </div>
        </div>
        <div
          className={`flex-auto border transition duration-500 ease-in-out ${
            step.completed ? "border-white" : "border-zinc-700"
          }`}
        />
      </div>
    );
  });

  return (
    <div className="mx-4 p-4 flex justify-between items-center">
      {stepsDisplay}
    </div>
  );
}

Stepper.propTypes = {
  steps: PropTypes.arrayOf(PropTypes.string).isRequired,
  currentStep: PropTypes.number.isRequired,
};
export default Stepper;
