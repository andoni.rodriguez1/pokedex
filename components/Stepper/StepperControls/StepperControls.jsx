/* eslint-disable react/forbid-prop-types */
import PropTypes from "prop-types";
import Button from "@/components/Button";
import { useStepperContext } from "@/context/StepperContext";

function StepperControls({ handleClick, currentStep, steps }) {
  const { teamData, mode } = useStepperContext();

  // disbale next button depending of the current step requirements
  const disableNext = () => {
    if (currentStep === 0 && (!teamData.teamName || !teamData.description)) {
      return true;
    }

    if (currentStep === 1 && !teamData.region.name) {
      return true;
    }

    if (currentStep === 2 && teamData.pokemons.length < 3) {
      return true;
    }

    return false;
  };

  return (
    <div className="mt-4 mb-8 flex justify-around">
      <Button
        type="button"
        onClick={() =>
          currentStep === 0 ? handleClick("cancel") : handleClick()
        }
        className={`cursor-pointer py-2 px-4 font-semibold uppercase transition duration-200 ease-in-out 
      hover:bg-zinc-500 hover:text-white  ${
        currentStep === 0 ? "text-red-400 bg-red-100" : "text-white bg-zinc-700"
      }`}
      >
        {currentStep === 0 ? "Cancel" : "Back"}
      </Button>

      {mode === "edit" ? (
        <Button
          type="button"
          onClick={() => handleClick("delete")}
          className="py-2 px-4 font-semibold uppercase transition duration-200 ease-in-out text-red-400 bg-red-100"
        >
          Delete Team
        </Button>
      ) : null}

      <Button
        type="button"
        onClick={() => handleClick("next")}
        disabled={disableNext()}
        className={`py-2 px-4 font-semibold uppercase bg-zinc-700 ${
          disableNext() ? "opacity-20 cursor-not-allowed" : "cursor-pointer"
        } transition duration-200 ease-in-out hover:bg-zinc-500`}
      >
        {currentStep === steps.length - 1 ? "Save Team" : "Next"}
      </Button>
    </div>
  );
}

StepperControls.propTypes = {
  handleClick: PropTypes.func.isRequired,
  currentStep: PropTypes.number.isRequired,
  steps: PropTypes.array.isRequired,
};

export default StepperControls;
