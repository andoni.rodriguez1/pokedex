import { useState } from "react";
import PropTypes from "prop-types";
import Image from "next/image";
import Badge from "@/components/TypeBadge/TypeBadge";
import Button from "@/components/Button";
import NoPokemonImage from "@/assets/images/no_pokemon_image.png";
import Modal from "@/components/Modal";
import { ImCheckboxChecked } from "react-icons/im";
import PokemonDetailView from "../PokemonDetailVew";

function Pokemon({ pokemon, onAddToTeamClick, addDisabled, isPokemonOnTeam }) {
  const [isPokemonDetailModalOpen, setIsPokemonDetailModalOpen] =
    useState(false);

  return (
    <div
      data-testid="pokemon"
      className="max-w-xs border border-zinc-700 rounded-lg shadow-md bg-zinc-800 mx-auto"
    >
      <div className=" flex flex-col items-center pb-10">
        <div className="pt-2">
          <Image
            className={`${isPokemonOnTeam ? "opacity-25" : "opacity-100"}`}
            src={pokemon.sprites.front_default || NoPokemonImage}
            alt={pokemon.name}
            width={144}
            height={144}
            defaultValue={NoPokemonImage}
          />
        </div>

        <div className="flex flex-col gap-4 items-center">
          <h5 className="flex gap-4 items-center text-xl font-medium text-gray-900 dark:text-white capitalize">
            {pokemon.name}
            {isPokemonOnTeam ? (
              <ImCheckboxChecked className="text-green-500" />
            ) : null}
          </h5>

          <div className="flex flex-wrap justify-center gap-2">
            {pokemon.types.map((type) => (
              <Badge key={type.name} type={type.name} />
            ))}
          </div>
          <div className="flex flex-wrap justify-center items-center  gap-3 px-2">
            <Button
              className="items-center text-sm font-medium text-center text-white bg-zinc-600 rounded-lg active:bg-zinc-600 hover:bg-zinc-500 focus:ring-1 focus:outline-none focus:ring-zinc-500"
              onClick={() =>
                setIsPokemonDetailModalOpen(!isPokemonDetailModalOpen)
              }
            >
              Detail
            </Button>
            <Button
              testId="add-button"
              disabled={addDisabled || isPokemonOnTeam}
              className={`text-sm font-medium leading-5 text-white transition-colors duration-150 ${
                addDisabled || isPokemonOnTeam
                  ? "opacity-20 cursor-not-allowed"
                  : "bg-zinc-600 hover:bg-zinc-500 "
              }  border border-transparent rounded-lg active:bg-zinc-600 focus:outline-none focus:ring-1 focus:shadow-outline-zinc focus:ring-zinc-500`}
              onClick={onAddToTeamClick}
            >
              Add to team
            </Button>
          </div>
        </div>
      </div>
      <Modal
        title={pokemon.name}
        showModal={isPokemonDetailModalOpen}
        setShowModal={setIsPokemonDetailModalOpen}
      >
        <PokemonDetailView pokemonName={pokemon.name} />
      </Modal>
    </div>
  );
}

Pokemon.propTypes = {
  pokemon: PropTypes.shape({
    name: PropTypes.string.isRequired,
    sprites: PropTypes.shape({
      front_default: PropTypes.string,
    }).isRequired,
    types: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
      }).isRequired
    ).isRequired,
  }),
  onAddToTeamClick: PropTypes.func.isRequired,
  addDisabled: PropTypes.bool.isRequired,
  isPokemonOnTeam: PropTypes.bool,
};

Pokemon.defaultProps = {
  pokemon: {
    sprites: {
      front_default: "@/assets/images/no_pokemon_image.png",
    },
  },
  isPokemonOnTeam: false,
};

export default Pokemon;
