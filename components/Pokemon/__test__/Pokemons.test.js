import { render, screen } from "@testing-library/react";
import Pokemon from "../Pokemon";

const pokemon = {
  name: "bulbasaur",
  sprites: {
    front_default:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png",
  },
  types: [
    {
      name: "grass",
    },
    {
      name: "poison",
    },
  ],
};

describe("Pokemon", () => {
  it("renders correctly", () => {
    render(
      <Pokemon
        pokemon={pokemon}
        onAddToTeamClick={jest.fn()}
        addDisabled={false}
      />
    );

    expect(screen.getByTestId("pokemon")).toBeInTheDocument();
  });

  it("call onAddToTeamClick when add button is clicked", () => {
    const onAddToTeamClick = jest.fn();

    render(
      <Pokemon
        pokemon={pokemon}
        onAddToTeamClick={onAddToTeamClick}
        addDisabled={false}
      />
    );

    screen.getByTestId("add-button").click();

    expect(onAddToTeamClick).toHaveBeenCalledTimes(1);
  });

  it("disables add button when addDisabled is true", () => {
    render(
      <Pokemon pokemon={pokemon} onAddToTeamClick={jest.fn()} addDisabled />
    );

    expect(screen.getByTestId("add-button")).toBeDisabled();
  });
});
