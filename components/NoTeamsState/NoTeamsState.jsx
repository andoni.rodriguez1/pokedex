import Link from "next/link";
import Lottie from "lottie-react";
import noTeamsLottie from "@/assets/lotties/no_teams.json";

function NoTeamsState() {
  return (
    <div className="grow overflow-auto items-center flex flex-col gap-5">
      <Lottie animationData={noTeamsLottie} loop />
      <p className="tracking-widest text-center">
        It looks like you don&apos;t have any team yet <br />
        How about creating one?
      </p>
      <Link
        className="px-4 py-2 w-fit flex items-center justify-start gap-4 rounded-lg bg-zinc-200 text-black font-semibold hover:bg-zinc-50"
        href="/create-team"
      >
        Create New Team
      </Link>
    </div>
  );
}

export default NoTeamsState;
