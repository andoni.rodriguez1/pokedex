import { render, screen } from "@testing-library/react";
import Modal from "../Modal";

describe("Modal", () => {
  test("renders Modal component", () => {
    render(<Modal setShowModal={jest.fn()} showModal />);
    expect(screen.getByTestId("modal")).toBeInTheDocument();
  });

  test("renders Modal component with children", () => {
    render(
      <Modal setShowModal={jest.fn()} showModal>
        <h1>Modal</h1>
      </Modal>
    );
    expect(screen.getByTestId("modal")).toBeInTheDocument();
    expect(screen.getByTestId("modal-child")).toBeInTheDocument();
  });

  test("renders Modal hidden default", () => {
    render(
      <Modal setShowModal={jest.fn()} showModal={false}>
        <p>child</p>
      </Modal>
    );

    const child = screen.queryByTestId("modal-child");
    expect(child).not.toBeInTheDocument();
  });
});
