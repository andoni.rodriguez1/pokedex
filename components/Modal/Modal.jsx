import PropsTypes from "prop-types";
import { AiOutlineClose } from "react-icons/ai";
import Button from "../Button";

function Modal({ children, title, setShowModal, showModal }) {
  return (
    <div data-testid="modal">
      {showModal ? (
        <>
          <div
            data-testid="modal-child"
            className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
          >
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-zinc-800 outline-none focus:outline-none">
                <div className="flex items-center justify-between p-5 rounded-t">
                  <h3 className="text-3xl font-semibold capitalize">{title}</h3>
                  <Button
                    className="bg-zinc-700 ml-5 float-right leading-none font-semibold outline-none focus:outline-none hover:bg-zinc-500"
                    onClick={() => setShowModal(false)}
                  >
                    <AiOutlineClose size={16} />
                  </Button>
                </div>
                <div className="p-5">{children}</div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black" />
        </>
      ) : null}
    </div>
  );
}

Modal.propTypes = {
  children: PropsTypes.node,
  title: PropsTypes.string,
  setShowModal: PropsTypes.func.isRequired,
  showModal: PropsTypes.bool.isRequired,
};

Modal.defaultProps = {
  children: null,
  title: "",
};

export default Modal;
