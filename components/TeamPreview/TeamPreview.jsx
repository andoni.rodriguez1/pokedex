import PropTypes from "prop-types";
import TypeBadge from "@/components/TypeBadge/TypeBadge";
import Button from "@/components/Button";
import Image from "next/image";
import NoPokemonImage from "@/assets/images/no_pokemon_image.png";
import { IoMdTrash } from "react-icons/io";

const STATS_TO_MAP = {
  hp: "hp",
  attack: "attack",
};

function TeamPreview({ team, onRemoveFromTeamClick }) {
  return (
    <div className="flex flex-col gap-4">
      {team.map((pokemon, index) => (
        <div
          key={pokemon.name}
          className="flex flex-row items-center justify-between gap-3 rounded-lg bg-zinc-800"
        >
          <div className="flex flex-row items-center gap-4">
            <div className="w-16 h-16">
              <Image
                src={pokemon.sprites.front_default || NoPokemonImage}
                alt={pokemon.name}
                width={144}
                height={144}
                defaultValue={NoPokemonImage}
              />
            </div>
            <div className="flex flex-col gap-2">
              <div>
                <h5 className="text-xl font-medium text-gray-900 dark:text-white capitalize">
                  {pokemon.name}
                </h5>
                <div className="flex gap-2">
                  {pokemon.stats.map((stat) => {
                    if (STATS_TO_MAP[stat.name]) {
                      return (
                        <span
                          className="text-zinc-500 uppercase text-xs"
                          key={stat.name}
                        >
                          {stat.name}: {stat.base_stat}
                        </span>
                      );
                    }
                    return null;
                  })}
                </div>
              </div>
              <div className="flex flex-wrap gap-2">
                {pokemon.types.map((type) => (
                  <TypeBadge key={type.name} type={type.name} />
                ))}
              </div>
            </div>
          </div>
          <div className="flex flex-row items-center gap-4">
            <Button
              className="text-md font-medium leading-5 text-white transition-colors duration-150 bg-zinc-600 hover:bg-zinc-500 border border-transparent rounded-lg active:bg-zinc-600 focus:outline-none focus:ring-1 focus:shadow-outline-zinc focus:ring-zinc-500"
              onClick={() => onRemoveFromTeamClick(index)}
            >
              <IoMdTrash />
            </Button>
          </div>
        </div>
      ))}
    </div>
  );
}

TeamPreview.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  team: PropTypes.array.isRequired,
  onRemoveFromTeamClick: PropTypes.func.isRequired,
};

export default TeamPreview;
