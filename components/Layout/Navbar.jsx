import LogoutButton from "@/components/LogoutButton/LogoutButton";
import Link from "next/link";
import { useRouter } from "next/router";

import { MdCatchingPokemon } from "react-icons/md";

function Navbar() {
  const router = useRouter();

  if (router.pathname === "/login") {
    return null;
  }

  return (
    <div className="w-full">
      <div className="flex justify-between px-10 pt-4 pb-4 sticky top-0">
        <Link className="flex justify-center items-center gap-2" href="/teams">
          <MdCatchingPokemon size={48} />
          <h2 className="text-xl uppercase tracking-widest sm:text-3xl">
            Pokedex
          </h2>
        </Link>
        <LogoutButton />
      </div>
    </div>
  );
}

export default Navbar;
