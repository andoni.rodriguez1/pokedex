import { useState } from "react";
import PropTypes from "prop-types";
import { ImSpinner2 } from "react-icons/im";

function Button({
  className,
  type,
  loading,
  disabled,
  startIcon,
  endIcon,
  children,
  testId,
  onClick,
}) {
  const [animate, setAnimate] = useState(false);
  return (
    <button
      data-testid={testId}
      className={`px-4 py-2 flex items-center justify-start gap-4 rounded-lg ${className} ${
        animate ? " animate-click" : ""
      } `}
      // Already set default value for type
      // eslint-disable-next-line react/button-has-type
      type={type}
      onClick={() => {
        setAnimate(true);
        onClick();
      }}
      disabled={loading || disabled}
      onAnimationEnd={() => setAnimate(false)}
    >
      {loading ? <ImSpinner2 className="animate-spin" /> : startIcon}
      {children}
      {endIcon}
    </button>
  );
}

Button.propTypes = {
  className: PropTypes.string,
  type: PropTypes.oneOf(["button", "submit", "reset"]),
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  children: PropTypes.node,
  startIcon: PropTypes.node,
  endIcon: PropTypes.node,
  testId: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

Button.defaultProps = {
  className: "",
  type: "button",
  loading: false,
  disabled: false,
  children: null,
  startIcon: null,
  endIcon: null,
  testId: "custom-button",
};

export default Button;
