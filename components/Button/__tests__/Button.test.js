import { render, screen } from "@testing-library/react";
import Button from "@/components/Button";
import { BiLogOut } from "react-icons/bi";

const mockOnClick = jest.fn();
const MOCK_TEXT = "Click me!";

describe("Button", () => {
  it("renders correctly", () => {
    render(<Button onClick={mockOnClick}>{MOCK_TEXT}</Button>);

    expect(screen.getByTestId("custom-button")).toBeInTheDocument();
  });

  it("renders correctly with props", () => {
    const mockType = "submit";

    render(
      <Button type={mockType} onClick={mockOnClick}>
        {MOCK_TEXT}
      </Button>
    );

    expect(screen.getByTestId("custom-button")).toHaveAttribute(
      "type",
      mockType
    );
  });

  it("renders correctly with children", () => {
    render(<Button onClick={mockOnClick}>{MOCK_TEXT}</Button>);

    expect(screen.getByTestId("custom-button")).toHaveTextContent(MOCK_TEXT);
  });

  it("renders correctly with disabled", () => {
    render(
      <Button disabled onClick={mockOnClick}>
        {MOCK_TEXT}
      </Button>
    );

    expect(screen.getByTestId("custom-button")).toBeDisabled();
  });

  it("renders correctly with icon", () => {
    render(
      <Button
        startIcon={<BiLogOut title="custom-button-icon" />}
        onClick={mockOnClick}
      >
        {MOCK_TEXT}
      </Button>
    );

    expect(screen.getByTestId("custom-button")).toHaveTextContent(MOCK_TEXT);
    expect(screen.getByTitle("custom-button-icon")).toBeInTheDocument();
  });

  it("executes onClick function", () => {
    render(<Button onClick={mockOnClick}>{MOCK_TEXT}</Button>);

    screen.getByTestId("custom-button").click();

    expect(mockOnClick).toHaveBeenCalledTimes(1);
  });

  it("does not execute onClick function when disabled", () => {
    const notCalled = jest.fn();
    render(
      <Button disabled onClick={notCalled}>
        {MOCK_TEXT}
      </Button>
    );

    screen.getByTestId("custom-button").click();

    expect(screen.getByTestId("custom-button")).toBeDisabled();
    expect(notCalled).not.toHaveBeenCalled();
  });
});
