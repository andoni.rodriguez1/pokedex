/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      keyframes: {
        click: {
          "0%, 100%": { transform: "translateY(1px)" },
        },
      },
      animation: {
        click: "click 50ms ease-in-out",
      },
    },
  },
  plugins: [],
};
