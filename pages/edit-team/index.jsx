import WithPrivateRoute from "@/components/WithPrivateRoute";
import EditTeam from "./EditTeam";

export default WithPrivateRoute(EditTeam);
