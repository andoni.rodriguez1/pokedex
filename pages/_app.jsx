/* eslint-disable react/jsx-props-no-spreading */
import { useState } from "react";
import PropTypes from "prop-types";
import "../styles/globals.css";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import Navbar from "@/components/Layout/Navbar";
import WithTeamCreationContext from "@/components/WithTeamCreationContext/WithTeamCreationContext";

function App({ Component, pageProps }) {
  const [queryClient] = useState(() => new QueryClient());
  return (
    <QueryClientProvider client={queryClient}>
      <section className="flex flex-col h-screen">
        <Navbar />
        <Component {...pageProps} />
      </section>
      <ReactQueryDevtools />
    </QueryClientProvider>
  );
}

App.propTypes = {
  Component: PropTypes.element.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  pageProps: PropTypes.object.isRequired,
};

export default WithTeamCreationContext(App);
