import WithPrivateRoute from "@/components/WithPrivateRoute";
import MyTeams from "@/pages/teams";
import Head from "next/head";

function Pokedex() {
  return (
    <>
      <Head>
        <title>Pokedex</title>
      </Head>
      <MyTeams />
    </>
  );
}

export default WithPrivateRoute(Pokedex);
