import { useState } from "react";
import { useRouter } from "next/router";
import {
  TeamInfoForm,
  RegionSelection,
  PokemonSelection,
} from "@/components/TeamCreationSteps";
import { useAuthState } from "react-firebase-hooks/auth";
import { pokedexDb, auth } from "@/config/firebase.config";
import { ref, push, remove, set } from "firebase/database";
import Head from "next/head";
import StepperControls from "@/components/Stepper/StepperControls/StepperControls";
import Stepper from "@/components/Stepper/Stepper";
import { useStepperContext } from "../../context/StepperContext";

const STEP_NAMES = ["Team Information", "Region Selection", "Team Members"];

const displayStep = (currentStep) => {
  switch (currentStep) {
    case 0:
      return <TeamInfoForm />;
    case 1:
      return <RegionSelection />;
    case 2:
      return <PokemonSelection />;
    default:
      return <> </>;
  }
};

function CreateTeam() {
  const [currentStep, setCurrentStep] = useState(0);
  const router = useRouter();
  const { teamData, mode } = useStepperContext();
  const [user] = useAuthState(auth);

  const handleStepClick = (direction) => {
    if (direction === "cancel") {
      router.replace("/teams");
    }

    if (direction === "next" && currentStep < STEP_NAMES.length) {
      setCurrentStep((prev) => prev + 1);
    } else if (currentStep > 0) {
      setCurrentStep((prev) => prev - 1);
    }

    if (direction === "next" && currentStep === STEP_NAMES.length - 1) {
      if (mode === "edit") {
        const teamDataWithoutId = { ...teamData };
        delete teamDataWithoutId.id;
        set(
          ref(pokedexDb, `teams/${user.uid}/${teamData.id}`),
          teamDataWithoutId
        );
      } else {
        push(ref(pokedexDb, `teams/${user.uid}`), teamData);
      }
      router.replace("/teams");
    }

    if (direction === "delete") {
      remove(ref(pokedexDb, `teams/${user.uid}/${teamData.id}`));
      router.replace("/teams");
    }
  };

  return (
    <div className="flex flex-col px-10 flex-[2] overflow-auto">
      <Head>
        <title>{`Pokedex | ${mode === "edit" ? "Edit" : "Create"} Team`}</title>
      </Head>
      <div>
        <Stepper steps={STEP_NAMES} currentStep={currentStep} />
      </div>
      <div className="grow overflow-auto">{displayStep(currentStep)}</div>
      {currentStep !== STEP_NAMES.length && (
        <StepperControls
          handleClick={handleStepClick}
          currentStep={currentStep}
          steps={STEP_NAMES}
        />
      )}
    </div>
  );
}

export default CreateTeam;
