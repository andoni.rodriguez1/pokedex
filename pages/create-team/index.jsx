import WithPrivateRoute from "@/components/WithPrivateRoute";
import CreateTeam from "./CreateTeam";

export default WithPrivateRoute(CreateTeam);
