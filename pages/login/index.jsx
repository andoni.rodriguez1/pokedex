import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect } from "react";
import {
  auth,
  googleAuthProvider,
  facebookAuthProvider,
} from "@/config/firebase.config";
import useSocialLoginFirebase from "@/hooks/useSocialLoginFirebase";
import { useAuthState } from "react-firebase-hooks/auth";
import Button from "@/components/Button";
import { FcGoogle } from "react-icons/fc";
import { BsFacebook } from "react-icons/bs";
import LoginImage from "@/assets/images/login_image.png";
import { MdCatchingPokemon } from "react-icons/md";
import Head from "next/head";

function LoginPage() {
  const { handleLogin, loadingLogin } = useSocialLoginFirebase(auth);
  const [user, loading] = useAuthState(auth);
  const router = useRouter();

  useEffect(() => {
    if (!loading && user) {
      router.replace("/teams");
    }
  }, [loading, user]);

  if (loading || user) {
    return null;
  }

  return (
    <section className="flex flex-col-reverse  items-center justify-evenly min-h-screen px-4 md:flex-row">
      <Head>
        <title>Pokedex | Login</title>
      </Head>
      <div className="flex flex-col gap-5 items-center shrink-0">
        <MdCatchingPokemon size={200} />
        <h1 className="font-bold text-4xl uppercase tracking-widest">
          Pokedex
        </h1>
        <Button
          className="bg-white text-[#757575] font-semibold w-full"
          loading={loadingLogin}
          onClick={() => handleLogin(googleAuthProvider)}
          startIcon={<FcGoogle />}
        >
          Sign-in with Google
        </Button>
        <Button
          className="bg-[#1877F2] font-semibold w-full"
          loading={loadingLogin}
          onClick={() => handleLogin(facebookAuthProvider)}
          startIcon={<BsFacebook />}
        >
          Sign-in with Facebook
        </Button>
      </div>
      <div className="w-2/3 md:w-auto hidden md:block">
        <Image src={LoginImage} alt="Picture of the author" priority />
      </div>
    </section>
  );
}

export default LoginPage;
