import WithPrivateRoute from "@/components/WithPrivateRoute";
import NoTeamsState from "@/components/NoTeamsState";
import LoadingState from "@/components/LoadingState";
import { useAuthState } from "react-firebase-hooks/auth";
import { pokedexDb, auth } from "@/config/firebase.config";
import { get, ref } from "firebase/database";
import TeamList from "@/components/TeamList/TeamList";
import Link from "next/link";
import Head from "next/head";
import { useEffect, useState } from "react";
import { useStepperContext } from "@/context/StepperContext";

const TEAM_DATA_INITIAL_STATE = {
  teamName: "",
  description: "",
  region: {
    name: "",
    id: "",
  },
  pokemons: [],
};

function MyTeams() {
  const pokedexTeamsDbRef = ref(pokedexDb, "teams");
  const [teams, setTeams] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const { setTeamData, setMode } = useStepperContext();
  const [user] = useAuthState(auth);

  useEffect(() => {
    setTeamData(TEAM_DATA_INITIAL_STATE);
    setMode("");
    setIsLoading(true);
    get(pokedexTeamsDbRef, `teams/${user.uid}`)
      .then((snapshot) => {
        if (snapshot.exists()) {
          const mappedTeams = Object.entries(snapshot.val()[user.uid]).map(
            ([key, value]) => ({
              id: key,
              ...value,
            })
          );
          setTeams(mappedTeams);
        }
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  }, []);

  if (isLoading) {
    return <LoadingState />;
  }

  return (
    <section className="flex flex-col px-10 flex-[2] overflow-auto pb-10">
      <Head>
        <title>Pokedex | Teams</title>
      </Head>
      {teams?.length > 0 ? (
        <>
          <div className="flex flex-col items-center sticky bg-zinc-900 top-0 w-full pt-10 pb-5 gap-5">
            <h1 className="uppercase tracking-widest text-3xl font-semibold">
              Your Teams
            </h1>
            <Link
              className="px-4 py-2 w-fit flex items-center justify-start gap-4 rounded-lg bg-zinc-200 text-black font-semibold hover:bg-zinc-50"
              href="/create-team"
            >
              Create New Team
            </Link>
          </div>
          <TeamList teams={teams} />
        </>
      ) : (
        <NoTeamsState />
      )}
    </section>
  );
}

export default WithPrivateRoute(MyTeams);
