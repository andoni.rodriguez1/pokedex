import { gql } from "graphql-request";

const getRegions = gql`
  query getRegions {
    pokemon_v2_region {
      id
      name
    }
  }
`;

export default getRegions;
