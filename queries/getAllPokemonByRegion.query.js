import { gql } from "graphql-request";

const getAllPokemonByRegion = (regionId) => gql`
  query getPokemonsByRegion {
    pokemon_v2_region_by_pk(id: ${regionId}) {
      name
      pokemon_v2_pokedexes(limit: 1) {
        name
        pokemon_v2_pokemondexnumbers_aggregate {
          aggregate {
            count
          }
          nodes {
            pokemon_species_id
            pokemon_v2_pokemonspecy {
              name
              pokemon_v2_pokemons {
                pokemon_v2_pokemonabilities {
                  pokemon_v2_ability {
                    name
                    pokemon_v2_abilityeffecttexts(
                      where: { language_id: { _eq: 9 } }
                    ) {
                      short_effect
                    }
                  }
                }
                pokemon_v2_pokemontypes {
                  pokemon_v2_type {
                    name
                  }
                }
                pokemon_v2_pokemonstats {
                  pokemon_v2_stat {
                    name
                  }
                  base_stat
                  effort
                }
                pokemon_v2_pokemonsprites {
                  sprites
                }
              }
            }
            pokedex_number
          }
        }
      }
    }
  }
`;

export default getAllPokemonByRegion;
