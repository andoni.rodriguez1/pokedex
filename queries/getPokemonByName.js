import axios from "axios";

const getPokemonByName = async (name) =>
  axios.get(`${process.env.NEXT_PUBLIC_POKEAPI_REST_URL}/pokemon/${name}`);

export default getPokemonByName;
