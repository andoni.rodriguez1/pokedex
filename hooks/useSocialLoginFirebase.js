import { useState } from "react";
import { signInWithPopup } from "firebase/auth";
import { useRouter } from "next/router";

function useSocialLoginFirebase(auth) {
  const [loadingLogin, setLoadingLogin] = useState(false);
  const router = useRouter();

  const handleLogin = async (provider) => {
    setLoadingLogin(true);
    await signInWithPopup(auth, provider)
      .then(() => {
        setLoadingLogin(false);
        router.replace("/teams");
      })
      .catch(() => {
        setLoadingLogin(false);
      });
  };
  return { loadingLogin, handleLogin };
}

export default useSocialLoginFirebase;
